#include <stdbool.h>
#include <stdlib.h>

#include "asss.h"
#include "packets/koth.h"

static Imodman		*mm;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Igame		*game;
static Inet		*net;
static Iconfig		*cfg;
static Ilogman		*lm;

static int arenaKey  = -1;
static int playerKey = -1;
static void (*OldShipResetFunc)(const Target *target);

#ifdef NDEBUG
#define assertlm(x) ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

#define MAXPRIZES (128)

struct arenaData
{
	int neededKillsForPromotion;
	signed char prizes[MAXPRIZES]; // what to prize when a player gets promoted
	int prizesCount;
};

struct playerData
{
	Arena *arena;
	int killsWithoutDeath;
	bool promoted; //does the player have the KOTH symbol?
};

static struct arenaData* GetArenaData(Arena *arena);
static struct playerData* GetPlayerData(Player *p);
static void ReadArenaSettings(Arena *arena);
static void CleanUpArenaSettings(Arena *arena);
static void ArenaActionCB(Arena *arena, int action);
static void RemoveKOTH(Player *p);
static void KillCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green);
static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq);
static void PlayerActionCB(Player *p, int action, Arena *arena);
static void ShipReset(const Target *target);

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
	if (lm) lm->Log(L_ERROR | L_SYNC, "<promotion> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
	fullsleep(500);
	Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        struct arenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);

        return adata;
}

static struct playerData* GetPlayerData(Player *p)
{
        assertlm(p);
        struct playerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        if (p->arena && p->arena != pdata->arena)
        {
                pdata->arena = p->arena;
        	//...
        }
        return pdata;
}

static void ReadArenaSettings(Arena *arena)
{
	struct arenaData *adata;
	ConfigHandle ch;
	int pr;
	const char *s;

	adata = GetArenaData(arena);
	ch = arena->cfg;

	adata->neededKillsForPromotion = cfg->GetInt(ch, "promotion", "KillsForPromotion", 5);
	s = cfg->GetStr(ch, "promotion", "prizes");


	pr = 0;
	while (s && *s && pr < MAXPRIZES)
	{
		if ((*s >= '0' && *s <= '9') || *s == '-' || *s == '+')
		{
			adata->prizes[pr] = strtol(s, &s, 0);
			pr++;
		}

		s++;
	}
	adata->prizesCount = pr;
}

static void CleanUpArenaSettings(Arena *arena)
{
	struct arenaData *adata;
	adata = GetArenaData(arena);
}

// These callbacks are only present if we have attached to the arena
static void ArenaActionCB(Arena *arena, int action)
{
        if (action == AA_CONFCHANGED)
        {
                ReadArenaSettings(arena);
        }
}

static void RemoveKOTH(Player *p)
{
	struct playerData *pdata;
	struct S2CKoth pkt;

	pdata = GetPlayerData(p);
	if (pdata->promoted)
	{
		pdata->promoted = false;
		pkt.type = S2C_KOTH;
		pkt.action = KOTH_ACTION_REMOVE_CROWN;
		pkt.time = 0;
		pkt.pid = p->pid;
		net->SendToArena(p->arena, NULL, (byte*)&pkt, sizeof(pkt), NET_RELIABLE);
	}
}

static void KillCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	struct playerData *killerData, *killedData;
	struct arenaData *adata;
	struct S2CKoth pkt;
	adata = GetArenaData(arena);
	int pr;
	Target tgtP;
	tgtP.type = T_PLAYER;

	if (IS_STANDARD(killed))
	{
		killedData = GetPlayerData(killed);
		killedData->killsWithoutDeath = 0;

		RemoveKOTH(killed);
	}

	if (IS_STANDARD(killer) &&
	    IS_STANDARD(killed)) // ignore killing a fake player
	{
		killerData = GetPlayerData(killer);
		killerData->killsWithoutDeath++;

		if (!killerData->promoted && killerData->killsWithoutDeath >= adata->neededKillsForPromotion) // do promotion
		{
			killerData->promoted = true;
			tgtP.u.p = killer;

			pr = 0;
			while (pr < adata->prizesCount)
			{
				game->GivePrize(&tgtP, adata->prizes[pr], 1);
				pr++;
			}

			pkt.type = S2C_KOTH;
			pkt.action = KOTH_ACTION_ADD_CROWN;
			pkt.time = 0;
			pkt.pid = killer->pid;
			net->SendToArena(arena, NULL, (byte*)&pkt, sizeof(pkt), NET_RELIABLE);
		}
	}
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
        struct playerData *pdata;
	pdata = GetPlayerData(p);

        pdata->killsWithoutDeath = 0;
        RemoveKOTH(p);
}


static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	Link *link;
	Player *p2;
	struct playerData *pdata;
	struct S2CKoth pkt;

	if (action == PA_ENTERARENA)
	{
		pdata = GetPlayerData(p);
		pdata->killsWithoutDeath = 0;
		pdata->promoted = false;
	}
	else if (action == PA_ENTERGAME) // send the koth symbol for all players that have been promoted
	{
		pkt.type = S2C_KOTH;
		pkt.action = KOTH_ACTION_ADD_CROWN;
		pkt.time = 0;

		pd->Lock();
		FOR_EACH_PLAYER(p2)
		{
			if (!IS_STANDARD(p) || p->arena != p2->arena) continue;
			pdata = GetPlayerData(p2);
			if (pdata->promoted)
			{
				pkt.pid = p2->pid;
				net->SendToOne(p, (byte*)&pkt, sizeof(pkt), NET_RELIABLE);
			}
		}
		pd->Unlock();
	}
}

static void ShipReset(const Target *target)
{
	OldShipResetFunc(target);

	LinkedList players = LL_INITIALIZER;
        Link *l;

	pd->Lock();
        pd->TargetToSet(target, &players);
        for (l = LLGetHead(&players); l; l = l->next)
        {
        	RemoveKOTH(l->data);
        }
        pd->Unlock();
	LLEmpty(&players);
}

EXPORT const char info_promotion[] = "Promotion by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(aman   );
        mm->ReleaseInterface(pd     );
        mm->ReleaseInterface(game   );
        mm->ReleaseInterface(net    );
        mm->ReleaseInterface(cfg    );
        mm->ReleaseInterface(lm     );
}

EXPORT int MM_promotion(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm      = mm_;
                aman    = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                game    = mm->GetInterface(I_GAME            , ALLARENAS);
                net     = mm->GetInterface(I_NET             , ALLARENAS);
                cfg	= mm->GetInterface(I_CONFIG	     , ALLARENAS);
                lm 	= mm->GetInterface(I_LOGMAN	     , ALLARENAS);


                if (!aman || !pd || !game || !net || !cfg || !lm)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));

                if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
                {
                        if (arenaKey  != -1) // free data if it was allocated
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1) // free data if it was allocated
                                pd->FreePlayerData (playerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                OldShipResetFunc = game->ShipReset;
		game->ShipReset = ShipReset;

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
        	if (game->ShipReset != ShipReset) // some other module has overriden this to, but hasen't set it back yet
        		return MM_FAIL;

        	game->ShipReset = OldShipResetFunc;

                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);

                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
		ReadArenaSettings(arena);
		mm->RegCallback(CB_ARENAACTION, ArenaActionCB, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->RegCallback(CB_KILL, KillCB, arena);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
		mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, arena);
		mm->UnregCallback(CB_KILL, KillCB, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
                mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		CleanUpArenaSettings(arena);

                return MM_OK;
        }

        return MM_FAIL;
}

